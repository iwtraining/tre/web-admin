describe('List Vehicle', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000');
    });

    it('verificando listagem', () => {
        cy.get('[data-cy="menu-vehicle"]').click();

        cy.get('[data-cy="menu-vehicle-list"]').click();

        cy.contains('Veiculos');

        cy.contains('Celta');
        cy.contains('Classic');
    });
});