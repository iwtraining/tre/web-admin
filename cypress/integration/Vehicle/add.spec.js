describe ('Add vehicle', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/')
    })

    it('preenchendo o formulario', () => {

        let plate = 'HCX 2813';
        let model = 'Oroch';
        let make = 'Renault';
        cy.wait(1000);


        cy.get('[data-cy="menu-vehicle"]').click();

        cy.get('[data-cy="menu-vehicle-add"]').click();

        cy.contains('Novo Veiculo');
        cy.get('[data-cy="plate"]').type(plate);
        cy.get('[data-cy="make"]').type(make);
        cy.get('[data-cy="model"]').type(model);

        

        

        cy.get('[data-cy="btn-ok"]').click();

        cy.on('window:alert', () => true);

        cy.contains(plate);
        cy.contains(make);
        cy.contains(model);

    })
});