describe('Add Faq', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000');
    });

    it('preenchendo o formulario', () => {
        cy.wait(1000);

        cy.get('[data-cy="menu-faq"]').click();

        cy.get('[data-cy="menu-faq-add"]').click();

        cy.contains('Nova pergunta');

        cy.get('[data-cy="question"]').type('pergunta teste');

        cy.get('[data-cy="answer"]').type('resposta teste');

        cy.get('[data-cy="btn-ok"]').click();

        cy.on('window:alert', () => true);

        cy.contains('Perguntas frequentes');

        cy.contains('pergunta teste');
    });
});