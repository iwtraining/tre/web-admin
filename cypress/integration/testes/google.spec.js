describe('Acesso ao google', () => {
    beforeEach(() => {
        cy.visit('https://google.com');
    });

    it('Garantindo que o usuario chegou na pagina correta', () => {
        cy.contains('Pesquisa Google');

        cy.get('.gLFyf').type('TRE Ceará');

        cy.get('.gNO89b').click({multiple: true});

        cy.contains('Tribunal Regional Eleitoral do Ceará');
    });
});