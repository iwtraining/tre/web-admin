describe('Testar login do facebook', () => {
    beforeEach(() => {
        cy.visit('https://facebook.com');
    });

    it('login invalido', () => {
        cy.get('#email').type('chiquim@email.com');
        cy.get('#pass').type('12345678');

        cy.get('[name="login"]').click();
    });
});