describe('Add Zone', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000');
    });

    it('preenchendo o formulario de zona', () => {
        cy.get('[data-cy="menu-zone"]').click();

        cy.get('[data-cy="menu-zone-add"]').click();

        cy.contains('Nova Zona');

        cy.get('[data-cy="sede"]').type('Iguatu');
        cy.get('[data-cy="numero"]').type('13');
        cy.get('[data-cy="juiz"]').type('Juiz teste');
        cy.get('[data-cy="chefe_cartorio"]').type('Chefe de cartório teste');
        cy.get('[data-cy="telefone"]').type('99999-9999');
        cy.get('[data-cy="email"]').type('iguatu@teste.com');
        
        cy.get('[data-cy="btn-ok"]').click();

        cy.on('window:alert', () => true);

        cy.contains('Zonas');

        cy.contains('Iguatu');
    });
});