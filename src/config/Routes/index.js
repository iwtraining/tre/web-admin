import React from "react";
import { Route, Switch } from 'react-router-dom';
import RoutesAuthors from './RoutesAuthors';

const PaginaErro = React.lazy(() => import ('../../pages/PaginaErro'));
const Example = React.lazy(() => import ('../../pages/Example'));
const ListZones = React.lazy(() => import ('../../pages/Zone/List'));
const ListUsers = React.lazy(() => import ('../../pages/User/List'));
const AddUser = React.lazy(() => import ('../../pages/User/Add'));
const ListVehicles = React.lazy(() => import ('../../pages/Vehicle/List'));
const RecoveryPassword = React.lazy(() => import ('../../pages/RecoveryPassword'));
const AddVehicle = React.lazy(() => import ('../../pages/Vehicle/Add'));
const AddZona = React.lazy(() => import ('../../pages/Zone/Add'));
const ListFaq = React.lazy(() => import ('../../pages/Faq/List'));
const AddFaq = React.lazy(() => import ('../../pages/Faq/Add'));
const Home = React.lazy(() => import ('../../pages/Home'));
const Calendar = React.lazy(() => import ('../../pages/Calendar'));
const CalendarAnt = React.lazy(() => import ('../../pages/CalendarAnt'));

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/zonas" exact component={ListZones} />
      <Route path="/exemplo" exact component={Example} />
      <Route path="/usuarios" exact component={ListUsers} />
      <Route path="/usuarios/novo" component={AddUser} />
      <Route path="/veiculos" exact component={ListVehicles} />
      <Route path="/veiculos/novo" component={AddVehicle} />
      <Route path="/zonas/novo" component={AddZona} />
      <Route path="/recuperar-senha" component={RecoveryPassword} />
      <Route path="/faq" exact component={ListFaq} />
      <Route path="/faq/novo" component={AddFaq} />
      <Route path="/agenda" component={Calendar}/>
      <Route path="/agenda-2" component={CalendarAnt}/>

      <RoutesAuthors />

      <Route path="/*" component={PaginaErro} />
    </Switch>
  );
}
