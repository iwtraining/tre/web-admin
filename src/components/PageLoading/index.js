import {Spin} from "antd";
import "./styles.scss";

export default function PageLoading() {
    return (
        <div className="page-loading">
            <Spin size="large"/>
        </div>
    )
}