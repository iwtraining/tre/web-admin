import { Col, Divider, Row } from "antd";
import { Line, Pie } from "@ant-design/charts";
import { useEffect, useState } from "react";
import { API_URL } from "../../config/Api";
import {format} from "date-fns";

export default function Home() {
    const [usersData, setUsersData] = useState([]);
    const [usersByMonth, setUsersByMonth] = useState([]);

    // let date = new Date();
    // let month = parseInt(date.getMonth() < 10 ? `0${date.getMonth()}` : date.getMonth()) + 1;
    // let today = `${date.getDate()}/${month}/${date.getFullYear()}`;

    // let today = moment().format('DD/MM/YYYY');

    let today = format(new Date(), 'dd/MM/yyyy HH:mm:ss');

    let chartUserConfig = {
        data: usersData,
        angleField: 'quantity',
        colorField: 'type',
        appendPadding: 10,
        radius: 0.5,
        label: {
          type: 'spider',
          labelHeight: 60,
          content: '{name}\n{percentage}',
        },
        interactions: [{ type: 'element-selected' }, { type: 'element-active' }],
    };

    let chartByMonthConfig = {
        data: usersByMonth,
        xField: 'month',
        yField: 'quantity',
        label: {},
        point: {
          size: 5,
          shape: 'diamond',
          style: {
            fill: 'white',
            stroke: '#5B8FF9',
            lineWidth: 2,
          },
        },
        tooltip: { showMarkers: false },
        state: {
          active: {
            style: {
              shadowBlur: 4,
              stroke: '#000',
              fill: 'red',
            },
          },
        },
        xAxis: { tickCount: 7 },
        slider: {
          start: 1,
          end: 7,
        },
        interactions: [{ type: 'marker-active' }],
    };

    useEffect(() => {
        fetch(API_URL+"/reports")
            .then(response => response.json())
            .then(response => {
                setUsersData(response.users);
                setUsersByMonth(response.registers_by_month)
            })
            .catch(error => alert('erro de conexao'));
    }, []);

    return (
        <div>
            <h1>Dashboard</h1>  
            <h2>{today}</h2>
            <Divider/>
            
            <Row gutter={64}>
                <Col span={8}>
                    <h3>Usuários</h3>

                    <Pie {...chartUserConfig}/>
                </Col>
                
                <Col span={16}>
                    <h3>Cadastros por mês</h3>
                    <Line {...chartByMonthConfig}/>
                </Col>
            </Row>
        </div>
    )
} 