import {useState, useEffect} from "react";
import { Modal } from "antd";
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import {API_URL} from "../../config/Api";

export default function Calendar() {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [modalTitle, setModalTitle] = useState('');
    const [events, setEvents] = useState([]);

    useEffect(() => {
        fetch(API_URL+'/events')
            .then(response => response.json())
            .then(response => setEvents(response))
            .catch(error => alert('erro de conexao'));
    }, []);

    const eventClick = (event) => {
        setIsModalVisible(true);

        setModalTitle(event.event.title);
    }

    return (
        <div>
            <FullCalendar
                headerToolbar={{
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek'
                }}
                editable={true}
                selectable={true}
                eventClick={eventClick}
                nowIndicator={true}
                eventDragStart={() => alert('drag')}
                plugins={[ dayGridPlugin, timeGridPlugin, interactionPlugin ]}
                initialView="dayGridMonth"
                events={events}
            />

            <Modal title={modalTitle} visible={isModalVisible} onOk={() => setIsModalVisible(false)} onCancel={() => setIsModalVisible(false)}>
                <p>Some contents...</p>
                <p>Some contents...</p>
                <p>Some contents...</p>
            </Modal>
        </div>
    )
}