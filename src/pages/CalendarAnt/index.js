import { Calendar, Badge } from "antd";

export default function CalendarAnt() {
    const events = [
            { type: 'warning', content: 'This is warning event.' },
            { type: 'success', content: 'This is usual event.' },
            { type: 'error', content: 'This is error event.' },
    ];

    function getMonthData(value) {
        if (value.month() === 8) {
          return 1394;
        }
      }
      
    function monthCellRender(value) {
        const num = getMonthData(value);
        return num ? (
          <div className="notes-month">
            <section>{num}</section>
            <span>Backlog number</span>
          </div>
        ) : null;
      }

    const content = (value) => {
        const listData = events;

        if (value.format('DD/MM') !== '27/10') {
            return;
        }

        return (
            <ul className="events">
                {listData.map(item => (
                    <li key={item.content}>
                    <Badge status={item.type} text={item.content} />
                    </li>
                ))}
            </ul>
        );
    };

    return (
        <div>
            <Calendar dateCellRender={content} monthCellRende={monthCellRender}/>
        </div>
    )
} 