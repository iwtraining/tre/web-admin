## TUTORIAL DO GIT

#### Voltar pro branch principal (main)
 - git checkout main

#### Pra atualizar o seu branch (local) principal
 - git pull origin main

#### Serve pra criar um novo branch, e mudar pra ele
 - git checkout -b nome-do-novo-branch

//faz as alterações nos codigos, de acordo com o solicitaddo na issue

#### Adicionar (embala) as modificações
 - git add .

#### Documenta o que foi alterado
 - git commit -m "mensagem"

#### Envia (empurra) as modificações pro branch que você criou
 - git push origin nome-do-novo-branch

// Agora você precisa clicar/copiar o link e abrir o Merge Request
