# web-admin

# Como funciona

## Comandos disponiveis

Dentro do diretorio do projeto, execute:

### `yarn install` ou `npm install`
Para instalar as dependencias da aplicação (react, react-router-dom, bootstrap, ant-design, etc)

### `yarn start` ou `npm start`

Para executar a aplicação em modo desenvolvimento.\
Abra [http://localhost:3000](http://localhost:3000) para ver no navegador.

### `json-server db.json -p 8000`
Para executar o banco de dados simulado, no endereço [http://localhost:8000](http://localhost:8000)

### `json-server db.json -p 8000 -m node_modules/json-server-auth`
Para executar o banco de dados, com autenticação, no endereço [http://localhost:8000](http://localhost:8000)

### `yarn run cypress open` ou `npx cypress open`

Para executar o ambiente de testes do cypress.